# Olá, Mundo! 👋

Atualmente, estou cursando Bacharelado em Sistemas de Informação e estou apaixonado pela tecnologia. Estou focado em estudar PHP e você pode acompanhar minha evolução aqui no GitHub!

## Projetos Destacados
  
### [Gerador de Curriculo](https://github.com/MichelNsouza/GeradorCV/)

- [Demo](https://geradorcurriculo.vercel.app/)
- Sistema para gerar curriculos em PDF feito em Laravel.
- Estilos em Bootstrap.
- Deploy no Vercel, não armazena dados.
  
### [CMS de Notícias](https://github.com/MichelNsouza/LaravelCMS)

- [Demo](https://laravel-cms.vercel.app/)
- Sistema de gerenciamento de notícias feito em Laravel.
- Estilos em Bootstrap.
- Deploy no Vercel com DB PostgreSQL na Render.

### [To-Do List](https://github.com/MichelNsouza/ToDo)

- [Demo](https://to-do-list-michel.vercel.app/)
- CRUD clássico de lista de tarefas feito em Laravel.
- Estilos em Bootstrap.
- Deploy no Vercel com DB PostgreSQL na Render.

### Desafio UN1D3V Lanches

- [Demo](https://michelnsouza.github.io/desafioUnidev/)
- Replicação de uma tela de uma hamburgueria fictícia com HTML, CSS e JS.

### Desafio 10 - Curso em Vídeo sobre Android

- [Demo](https://michelnsouza.github.io/projetoAndroid/)
- Desafio proposto por Gustavo Guanabara no curso de HTML5 e CSS3.

### Desafio 12 - Curso em Vídeo de Cordel

- [Demo](https://michelnsouza.github.io/projetoCordel/)
- Outro desafio proposto por Gustavo Guanabara no curso de HTML5 e CSS3.

## Minha Trilha de Estudos Até o Momento

### Introdução ao Mundo DEV

- Curso HTML5 e CSS3 (módulos 1 ao 4 do Curso em Vídeo)
- Lógica de programação com JavaScript
- Arquitetura de computadores
- HTML e CSS: ambientes de desenvolvimento, estrutura de arquivos e tags
- HTML e CSS: Classes, posicionamento e Flexbox
- HTML e CSS: Cabeçalho, footer e variáveis CSS
- HTML e CSS: Trabalhando com responsividade e publicação de projetos
- HTML e CSS: Praticando HTML/CSS
- HTML e CSS: Responsividade com mobile-first

### Escolhendo Minha Primeira Linguagem

- C: conhecendo a Linguagem das Linguagens
- Desafios beecrowd em C/C++ (preparação para maratona de programação SBC 2022)
- Java JRE e JDK: compile e execute o seu programa
- Java OO: entendendo a Orientação a Objetos
- JavaScript para Web: Crie páginas dinâmicas
- PHP: conceitos, lidando com dados, loops e mais

### Aprofundando no Mundo DEV

- Windows Prompt: utilizando o CMD
- Linux Onboarding: usando a CLI de uma forma rápida e prática
- HTTP: Entendendo a web por baixo dos panos
- Git e GitHub: repositório, commit e versões
- Linux I: conhecendo e utilizando o terminal

### Mundo PHP

- Avançando com PHP: Arrays, Strings, Funções e Web
- PHP: manipulando coleções com Arrays
- PHP Strings: manipulando textos com PHP
- Orientação a Objetos com PHP: Classes, métodos e atributos
- Avançando com Orientação a Objetos com PHP: Herança, Polimorfismo e Interfaces
- PHP I/O: trabalhando com arquivos e streams
- PHP Exceptions: tratamento de erros

### Banco de Dados

- Modelagem de banco de dados: entidades, relacionamentos e atributos
- Modelagem de banco de dados relacional: modelagem lógica e física
- Modelagem de banco de dados relacional: normalização
- Modelagem de banco de dados relacional: entendendo SQL
- MySQL e JSON: persistindo JSON de maneira eficiente
- PostgreSQL: Introdução ao SGBD
- PostgreSQL: Views, Sub-Consultas e Funções
- PostgreSQL: comandos DML e DDL
- PostgreSQL: desenvolvendo com PL/pgSQL
- PostgreSQL: Triggers, transações, erros e cursores
- PostgreSQL: administração e otimização do banco

### Mundo PHP NVL 2

- PHP Composer: Dependências, Autoload e Publicação
- PHP e PDO: trabalhando com bancos de dados
- PHP e MySQL: criando sua primeira aplicação web
- PHP na Web: conhecendo o padrão MVC
- PHP na Web: lidando com segurança e API
- PHP na Web: aplicando boas práticas e PSRs

### Mundo Laravel

- Laravel: criando uma aplicação com MVC
- Acampamento Dev: Laravel To-Do List
- Acampamento Dev: Laravel Gerenciamento de Notícias
- Laravel: validando formulários, usando sessões e definindo relacionamentos
- Laravel: e-mails, eventos assíncronos, uploads e testes
- Laravel: transações, service container e autenticação
- Atualmente elaborando projetos para consolidar o conhecimento :) 

## Vamos nos conectar no LinkedIn?

Você pode me encontrar em [Michel Souza](https://www.linkedin.com/in/michel-n-souza/).

Lembre-se: "Conhecimento sem visão só te faz mais um burro convicto!" ~ Djonga.
